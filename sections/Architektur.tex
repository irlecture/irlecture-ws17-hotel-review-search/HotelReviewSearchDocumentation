%%%%%%%%%%%%%%%%%%%%%%
%
% Architektur
%
%%%%%%%%%%%%%%%%%%%%%%

\section{Architektur}
\label{architektur}

In diesem Kapitel wird näher auf die Architektur der Suchmaschine eingegangen. Abbildung~\ref{fig:architekturskizze-hotelreviewsearch} zeigt dabei die Architekturskizze mit den einzelnen Komponenten und deren Zusammenhang als Überblick über das System.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{img/Architekturskizze-HotelReviewSearch}
	\caption{Architekturskizze des Systems}
	\label{fig:architekturskizze-hotelreviewsearch}
\end{figure}

\subsection{Technologieauswahl}
\label{technologie-auswahl}

Die domänenspezifische Suchmaschine wurde mit der Programmiersprache \textit{Java} umgesetzt. Hintergrund für den Einsatz von Java ist die Verwendung von \textit{Apache Lucene}, einer hochperformante Bibliothek zur Volltextsuche, welche eine umfangreiche Java-API implementiert. \textit{Lucene} steht unter der \textit{Apache-Lizenz} und ist eine frei verfügbare Software, welche durch die \textit{Apache Software Fundation} bereitgestellt wird. Der Such- und Indizierungsprozess wurde damit vollständig umgesetzt.

Für die Erstellung der Nutzerschnittstelle wurde das plattformunabhängige \textit{Spring Framework} verwendet. Dieses bietet ein breites Spektrum an Funktionalität und stellt unter anderem das Entwurfsmuster \textit{Model-View-Controller (MVC)} bereit. Dadurch lässt sich die Software in logische Komponenten trennen und fördert somit die Erweiterbarkeit und Wartbarkeit.

Zur Verwaltung des Projektes wurde \textit{Maven}, ein auf \textit{Java} basierendes Build-Management-Tool, verwendet. \textit{Maven} gehört ebenfalls zur \textit{Apache Software Fundation} und ist frei verfügbar. Neben der Verwaltung von Informationen und dem Erstellen des Projektes, stellt es ein Dependency-Management zur Verfügung, mit welchem Abhängigkeiten zu weiteren Artefakten, wie \textit{Apache Lucene}, definiert werden können.

\subsection{Dokumentkollektion}
\label{schema-datensatz}

Als Dokumentkollektion wurden Daten der Hotel-Website \textit{TripAdvisor} verwendet, welche innerhalb des \textit{Webis TripAdvisor Corpus 2014}\footnote{\url{https://www.uni-weimar.de/en/media/chairs/computer-science-department/webis/data/corpus-webis-tripad-14/} (aufgerufen am 22.03.2018)} als Datensatz zur freien Verfügung stehen. Der Korpus enthält ca. 12.000 Hotels denen ca. 266.000 Hotelbewertungen (Reviews) zugeordnet sind. Eine Hotel-Instanz ist als JSON-Datei repräsentiert und beinhaltet neben Informationen zu dem Hotel, wie bspw. dem Hotelnamen und einer Hotelbeschreibung, im Durchschnitt 22 Reviews. Ein Review ist als JSON-Objekt innerhalb des Hotel-Dokuments modelliert und beinhaltet neben dem Namen des Nutzers, einen Titel der Bewertung und verschiedenen Bewertungsangaben auch einen Freitext, welcher einen wichtigen Bestandteil für die Indizierung darstellt. Darin berichtet der Nutzer von positiven und negativen Erlebnissen während des Hotel-Aufenthalts. Der Text weist oftmals Rechtschreib- und Grammatikfehler auf. Listing~\ref{lst:reviewschema} zeigt das Schema einer solchen JSON-Datei.

% LISTING
\lstset{style=json-style, caption=Schema Review, label=lst_schema_review, numbers=left, numberstyle=\normalfont,caption={Schema einer JSON-Datei aus der Dokumentkollektion},captionpos=b,label=lst:reviewschema}
\lstinputlisting[]{listings/lst_schema_review}

Für die hier betrachtete Suchmaschine wurde sich für zwei verschiedene Dokument-Typen entschieden. Einerseits ist ein Hotel, inklusive dessen Metadaten und Reviews, ein Dokument. Andererseits ist auch ein Review mit allen Bewertungsinformationen und einer Referenz auf das Hotel ein Dokument. Die Begründung folgt in den Erläuterungen zur Indizierung im nächsten Abschnitt.

\subsection{Web User-Interface}
\label{frontend}

Das Web-basierte User-Interface kann in drei logisch voneinander getrennte Ansichten unterteilt werden: (1) eine Startseite mit Eingabefeld für einen Suchbegriff, (2) eine Seite zur Auflistung der Suchergebnisse und (3) einer Detail-Seite mit Informationen zu einem Hotel inklusive einer Auflistung dessen Bewertungen. Als Webserver-Instanz wird die Java-Bibliothek \textit{SpringBoot} verwendet. Die HTML-Seiten (\texttt{index.html}, \texttt{search.html}, \texttt{detail.html}) sind Templates, welche mit der Templating-Engine \textit{Tyhmeleaf} server-seitig mit dem entsprechenden Inhalt gefüllt werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{img/index-page}
	\caption{Startseite mit Eingabefeld für einen Suchbegriff}
	\label{fig:index-page}
\end{figure}

Nach Betätigung des Such-Buttons auf der Startseite (siehe Abbildung~\ref{fig:index-page}) wird ein HTTP-Request an den Webserver gesendet, welcher durch einen entsprechenden SpringBoot Controller verarbeitet wird. Der Suchstring wird als Query-Parameter innerhalb der URL übertragen. Durch die Implementation einer \texttt{RequestInterceptor}-Klasse konnten alle über SpringBoot empfangenen Requests vor der Verarbeitung durch den Controller ausgelesen und in einer Log-Datei protokolliert werden. Somit werden bspw. IP-Adresse, Request-URL, Query und Antwortzeit für jede Suchanfrage gespeichert. Über die im nächsten Abschnitt beschriebene Suchfunktion werden die Top-10 Hotels ermittelt und als Ergebnisse in die entsprechende Seite (2) eingetragen. Die Darstellung der Ergebnisse erfolgt durch Snippets, welche die Hotelbeschreibung und die drei relevantesten Hotelbewertungen zur Suchanfrage enthält. Der Suchbegriff wird innerhalb der Snippets hervorgehoben (siehe Abbildung~\ref{fig:search-page}). Durch eine Pagination-Bar im unteren Bereich der Seite kann zwischen den verschiedenen Seiten gewechselt werden, falls der Nutzer mehr als die Top-10 Hotels betrachten möchte. Die Pagination erfolgt Server-seitig mit Lucene.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{img/search-page}
	\caption{Seite zur Auflistung der Suchergebnisse}
	\label{fig:search-page}
\end{figure}

Jeder Titel eines Hotels entspricht einem Link auf eine dynamisch generierte Detail-Seite, welche zunächst alle zur Verfügung stehenden Hoteleigenschaften (z. B. Hotelbeschreibung, Land, Stadt, Anzahl an Reviews und eine akkumulierte Übersicht der Nutzerbewertungen) anzeigt (siehe Abbildung~\ref{fig:detail-page}). 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.85\linewidth]{img/detail-page}
	\caption{Detail-Seite mit Hotelinformationen und Bewertungen}
	\label{fig:detail-page}
\end{figure}

Darunter werden alle zu dem Hotel vorhandenen Gästebewertungen aufgelistet. Die Hotelbewertungen, die als Treffer zum angegebenen Suchstring gefunden wurden, werden farblich hervorgehoben und sind nach Relevanz sortiert. 

\subsection{Indizierungsprozess}
\label{indizierungsprozess}
Ein Hauptbestandteil der implementierten Suchmaschine ist der Indizierungsprozess, welcher nachfolgend näher betrachtet wird.

\paragraph{Datenaufbereitung}
Durch genauere Betrachtung der Dokumente des Korpus wurde festgestellt, dass einige wichtige Keys nicht in allen Dokumenten gesetzt waren. Insbesondere die Information in welcher Stadt sich ein Hotel befand war nicht überall vorhanden. Aus diesem Grund wurde u.~a. versucht, die fehlenden Städte zu bestimmen. Hierfür wurde nach Städtenamen in den Feldern \textit{description, name} und \textit{aka} gesucht.

Die Informationsextraktion der Städtenamen (\textit{Named Entities}) wurde mit einem \textit{Python}-Programm realisiert. Dieses implementiert eine Bibliothek namens \textit{GeoText}, welche wiederum auf www.geonames.org zurückgreift, um in den genannten Felder Städte zu finden. Weiterhin wurde begonnen, mit einem weiteren Skript fehlende Städtenamen auf \textit{www.tripadvisor.com} über eine Suche des Hotelnamens zu beziehen.

Damit die zusätzlichen Daten gespeichert werden konnten, wurden die Dokumente um entsprechend Schlüssel-Wert-Paare erweitert. Zuvor wurden alle Schlüssel-Wert-Paare gemäß der \textit{Readme}-Datei des bereitgestellten Datensatzes normalisiert, d.~h. fehlende Schlüssel wurden hinzugefügt und deren Wert mit \texttt{null} definiert. Die Aufgaben und Bearbeitungsreihenfolge der implementierten \textit{Python}-Anwendung kann wie folgt zusammengefasst werden:

\begin{enumerate}
	\item Normalisieren der Dokumente (Auffüllen nicht vorhandener Schlüssel-Wert-Paare gemäß Schema)
	\item Erweiterung um notwendige Schlüssel für die Städtesuche und \textit{TripAdvisor}-Daten
	\item Bestimmen der Städtename aus den Feldern \textit{description, title} und \textit{aka}
	\item Suche bei \textit{TripAdvisor} nach Städtenamen (nicht vollständig abgeschlossen)
\end{enumerate}

Zu Beginn bestand die Idee, fehlende Städtenamen auch aus den Review-Texten zu extrahieren. Dabei ist aufgefallen, dass bspw. das Wort \textit{nice} sich sowohl auf die Stadt \textit{Nizza} beziehen kann, als auch auf das gleichnamige Adjektiv. Aus diesem Grund wurde die Idee verworfen. Weiterhin kann die  Informationsextraktion möglicherweise mit anderen Bibliotheken verbessert werden. Hierfür würde sich bspw. das \textit{Natural Language Toolkit (NLTK)\footnote{https://www.nltk.org/\_modules/nltk/tag/stanford.html}} der Universität Stanford eignen.

\paragraph{Indizierung}
Für die Hotel-Review-Suchmaschine wurden drei verschiedene Indizes erzeugt, um eine möglichst hohe Effizienz zu gewährleisten und unterschiedliche Dokumenttypen voneinander zu trennen. Abbildung~\ref{fig:index-strukturen} zeigt eine Übersicht der Indizes. Der \textit{Hotel-Review-Index} beinhaltet alle Hotel-Instanzen mit den jeweiligen Reviews. Ein Index über alle einzelnen Reviews mit einem Verweis auf das entsprechende Hotel stellt der \textit{Review-Index} dar. Innerhalb des \textit{Request-Index} werden bereits getätigte Suchanfragen indiziert.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\linewidth]{img/Index-Strukturen.pdf}
	\caption{Überblick Indizes}
	\label{fig:index-strukturen}
\end{figure}

Das Erzeugen der drei Indizes wird durch die Java Klasse \textit{IndexMain} initiiert. Für jeden Index ist eine \textit{Index-Engine} verantwortlich, die den entsprechenden Index erzeugt. Diese erben von der \textit{AbstractIndexEngine}, welche die generelle Datenstruktur eines hier benötigten Indexes beschreibt.

Der \textit{HotelReviewIndex} wird benötigt um zur Nutzeranfrage passende Hotels zu finden. Dabei werden Dokumente, wie in Abbildung \ref{fig:index-strukturen} (links) zu sehen, erzeugt, welche Hotelinformationen (wie z.B. HotelID, Name, Description, Pfad zur korrespondierenden JSON-Datei) sowie alle Review-Titel und dazugehörige Review-Texte enthalten. Auf Grundlage dieses Indexes wird bei einer Anfrage zunächst nach passenden Hotels gesucht und nach Relevanz sortiert. Die Datenstruktur des \textit{ReviewIndex} ermöglicht das Ranken der Top $y$ Reviews pro Hotel. Dieser enthält für jedes Review ein Dokument, bestehend aus den Feldern wie in Abbildung \ref{fig:index-strukturen} (mitte) zu sehen ist. Der \textit{RequestIndex} wird verwendet, um während des Eingebens der Anfrage Suchvorschläge (Suggestions) zu generieren und dem Nutzer anzubieten. Zudem wird dieser um die abgesendeten Queries erweitert.

\subsection{Suchprozess}
\label{suchprozess}

Bei der hier betrachteten Suchmaschine existieren zwei voneinander getrennte Suchprozesse, welche unterschiedlich auf die vorhandenen Indizes (siehe Abschnitt~\ref{indizierungsprozess}) zugreifen. Zum einen ist das die Suche nach Hotel-Entitäten für die Ergebnisseite (nachfolgend \textit{Hotelsuche} genannt) und zum anderen die Suche nach den Top-Reviews für ein bestimmtes Hotel für die Detail-Seite (nachfolgend \textit{Review-Suche} genannt).

\paragraph{Hotelsuche} Ziel dieser Suche ist es, die Top-10 Hotels für eine auf der Startseite eingegebene Suchanfrage zu finden. Da für diese Suche sowohl die Informationen zu dem Hotel (Hotelname und Beschreibung) als auch die Inhalte der einzelnen Bewertungen relevant sind, wird der \textit{Hotel-Review-Index} verwendet. Man erhält eine nach Relevanz sortierte Liste von 10 Index-Dokumenten, wobei ein Dokument aus der Hotel-Entität inklusive aller zugehörigen Reviews in unsortierter Reihenfolge besteht. Damit im User-Interface unter jedem Hotel-Namen und dessen Beschreibung Ausschnitte der Top-3 Reviews für das entsprechende Hotel angezeigt werden kann, wird pro Hotel eine zusätzliche Suche auf dem \textit{Review-Index} ausgeführt.\footnote{Eine mögliche Verbesserung dieses Vorgehens ist ausschließlich eine Query zu definieren, welche die Reviews für alle 10 Hotel-Ids zurückliefert, anstelle von 10 voneinander getrennten Suchanfragen.} Die Hotel-Id und der Suchtext werden über eine \texttt{BooleanQuery} zusammengeführt, damit die Hotel-Id ausschließlich auf dem Feld \textit{Hotel-Id (IntPoint)} und die Suchanfrage auf den Feldern \textit{Review-Titel} und \textit{Review-Text} des Indexes ausgeführt werden. Hier kommt auch der Vorteil eines \textit{IntPoint}-Feldes zum Einsatz, da mit Hilfe eines von \texttt{IntPoint.newExactQuery(\{field-name\},\{value\})} erzeugten \texttt{Query}-Objektes nur die Reviews herangezogen werden, welche die angegebene Hotel-Id aufweisen.

\paragraph{Review-Suche} Wählt der Nutzer ein Hotel im User-Interface aus, wird die im Abschnitt~\ref{frontend} vorgestellte Detail-Seite angezeigt. Darin werden die zu der Query passenden Top-10 Reviews nach Relevanz sortiert sowie farbig markiert ausgegeben. Mit der Hotel-Id und dem Suchstring wird eine Suche auf dem \textit{Review-Index} ausgeführt. Analog zum oben beschriebenen Verfahren wird hier auch das \textit{IntPoint}-Feld zur Festlegung der Hotel-Id genutzt. Da in einem Dokument dieses Indexes auch der Pfad zu der JSON-Datei des Hotels abgelegt ist, können darüber auch alle weiteren Reviews, die sich nicht unter den Top-10 befinden, extrahiert und auf der Detail-Seite angezeigt werden.

Die hier beschriebene Suche ist Bestandteil des ersten Meilensteins des Projektes. Die Evaluation der Effektivität dieses Meilensteins ist im Kapitel~\ref{evaluation} dargelegt. Dieser Suchprozess wurde im Zuge eines 2. Meilensteins noch optimiert. Die Bestandteile dieser Optimierung sind im Kapitel~\ref{optimierung} zusammengefasst. Nach Implementation der Verbesserungen wurde ein weitere Messung der Effektivität durchgeführt, deren Ergebnis ebenfalls im Kapitel der Evaluation enthalten ist.

\subsection{Suggestion-Service}
\label{suggestionservice}

Sobald der Benutzer etwas in das Eingabefeld für die Suche nach Hotel-Reviews eingegeben hat, werden ihm Vorschläge zur automatischen Vervollständigung seiner Suchanfrage gemacht.
Diese automatischen Suchvorschläge werden durch den \textit{Suggestion-Service} unterbreitet.
Abbildung~\ref{fig:suggestions-screenshot} zeigt, wie die Vorschläge dem Benutzer präsentiert werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{img/suggesions}
	\caption{Vorschläge für die automatische Vervollständigung der Suchanfrage}
	\label{fig:suggestions-screenshot}
\end{figure}

Auf Seiten des Frontends wurde der Suggestion-Prozess mit Hilfe von \textit{Twitters \mbox{typeahead.js}} und dessen \textit{Bloodhound Suggestion Engine}\footnote{\url{https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md} (aufgerufen am 24.03.2018)} realisiert.
Nach Eingabe eines oder mehrerer Zeichen, kann der Benutzer aus zwei Vorschlagslisten mit maximal fünf Elementen wählen.
Die erste Liste basiert dabei auf der Dokumentenkollektion selbst.
Die zweite Liste wird anhand protokollierter Suchanfragen erzeugt.
Auf  Seiten des Backends nimmt ein REST-Service die Anfragen der \textit{Bloodhound Suggestion Engine} entgegen und erzeugt die beiden Vorschlagslisten.
Im Folgenden wird das Vorgehen für beide Listen näher erläutert.

\paragraph{Korpusbasierte Suggestions}

Die Vorschlagsliste auf Basis der Dokumentenkollektion verwendet \textit{Lucenes FuzzySuggester}.
Zunächst wird ein Iterator über alle Terme des Index-Felds \textit{Review-Text} aus dem \textit{Hotel-Review-Index} erstellt.
Jeder Term im Iterator erhält seine absolute Termhäufigkeit, innerhalb aller \textit{Review-Text} Felder, als Gewicht für die \textit{Suggestion} Funktion.
Dieser Iterator wird nun dem \textit{FuzzySuggester} übergeben.
Somit erhält der Benutzer für seine Eingabe Vorschläge, für die tatsächlich häufigsten Wörtern dieser Domäne.
Werden mehrere Terme bei der Suche eingegeben, so wird in der Vorschlagsliste für jeden Term der beste Treffer übernommen und für den letzten wieder 5 Vorschläge unterbreitet.
Durch die Verwendung des \textit{FuzzySuggesters} stellen die Vorschläge auch gleichzeitig eine Rechtschreibkorrektur dar (vgl.~Abb.~\ref{fig:suggestion-spelling-screenshot}).

\begin{figure}[h]
	\centering
	\includegraphics[width=0.23\linewidth]{img/suggestion-spelling}
	\caption{Rechtschreibkorrektur durch den \textit{FuzzySuggester}}
	\label{fig:suggestion-spelling-screenshot}
\end{figure}

\paragraph{Protokollbasierte Suggestions}

Die zweite Vorschlagsliste basiert auf den protokollierten Suchanfragen der Suchmaschinenbenutzer.
Jede abgesendete Anfrage wird in einem Query-Log protokolliert.
Diese Log-Datei wird zum Zeitpunkt der initialen Index-Erstellung eingelesen, um daraus einen eigenen \textit{Request-Index} zu erstellen.
Zur Laufzeit der Web-Anwendung werden neue Suchanfragen asynchron in den \textit{Request-Index} aufgenommen.
Für die erzeugung der Vorschlagsliste wird wieder \textit{Lucenes FuzzySuggester} verwendet.
Anders als bei den korpusbasierten Suggestions verwendet dieser hier jedoch keinen Iterator als Datenbasis.
Bei den protokollbasierten Suggestions ist der Suggester \textit{Dictionary-basiert}.
Er erhält also bei seiner Initaliserung den \textit{Request-Index} als Datenbasis.
Bei dieser Vorgehensweise werden allerdings die zur Laufzeit neu zum Index hinzugefügten Suchanfragen nicht mit berücksichtigt.
Damit diese dennoch als Vorschlag unterbreiten werden können, wurde ein \textit{Suggestion Refresh Daemon} implementiert.
Dieser erneuert in einem regelmäßigen Zeitintervall asynchron den Suggester für die protokollbasierten Suchvorschläge.